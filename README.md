Basic Swagger API for todo app in Symfony 4

Endpoints
---------

 * Task
 * Comment (comments can be added to different contexts)


Cache
-----

* Doctrine query result
  * Task::findByIdentifier