<?php

namespace App\Controller\Api;

use App\Entity\Comment;
use App\Form\Comment\CreateCommentType;
use App\Form\Comment\UpdateCommentType;
use App\Model\Comment\CreateCommentRequest;
use App\Model\Comment\UpdateCommentRequest;
use App\Service\CommentManager;
use App\Service\CommentProvider;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * @Route("/api/comment")
 */
class CommentController extends Controller
{

    /**
     * Get all comments in context
     *
     * @Route("/{context}/{contextIdentifier}", methods={"GET"})
     * @SWG\Parameter(
     *     name="context",
     *     in="path",
     *     description="Context type",
     *     type="string",
     *     enum={"task"},
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="contextIdentifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Return comments",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Comment::class, groups={"simple"})
     *     )
     * )
     * @SWG\Response(
     *     response="400",
     *     description="Wrong context",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not supported context",
     * )
     * @SWG\Tag(name="Comment")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param string $context
     * @param string $contextIdentifier
     * @param CommentProvider $commentProvider
     * @return View
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \App\Exception\NotExistsException
     */
    public function list(string $context, string $contextIdentifier, CommentProvider $commentProvider): View
    {
        return View::create(['comments' => $commentProvider->getAllByContext($context, $contextIdentifier)]);
    }

    /**
     * Add new comment in context
     *
     * @Route("/{context}/{contextIdentifier}", methods={"POST"})
     * @SWG\Parameter(
     *     name="context",
     *     in="path",
     *     description="Context type",
     *     type="string",
     *     enum={"task"},
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="contextIdentifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="comment",
     *     in="body",
     *     description="Add comment",
     *     required=true,
     *     @Model(type=CreateCommentRequest::class)
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Return created comment",
     *     @SWG\Schema(
     *         @Model(type=Comment::class, groups={"simple"})
     *     )
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not supported context",
     * )
     * @SWG\Tag(name="Comment")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param Request $request
     * @param string $context
     * @param string $contextIdentifier
     * @param DecoderInterface $decoder
     * @param CommentManager $commentManager
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(Request $request, string $context, string $contextIdentifier, DecoderInterface $decoder, CommentManager $commentManager): View
    {
        $command = new CreateCommentRequest();

        $data = $decoder->decode($request->getContent(), 'json');

        $form = $this->createForm(CreateCommentType::class, $command);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $comment = $commentManager->addComment($context, $contextIdentifier, $command);

            return View::create(['comment' => $comment]);
        }

        return View::create(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Update comment
     *
     * @Route("/{context}/{contextIdentifier}/{identifier}", methods={"PATCH"})
     * @SWG\Parameter(
     *     name="context",
     *     in="path",
     *     description="Context type",
     *     type="string",
     *     enum={"task"},
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="contextIdentifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="identifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="comment",
     *     in="body",
     *     description="Update comment",
     *     required=true,
     *     @Model(type=UpdateCommentRequest::class)
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Return comments",
     *     @SWG\Schema(
     *         @Model(type=Comment::class, groups={"simple"})
     *     )
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * )
     * @SWG\Tag(name="Comment")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param Request $request
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @param DecoderInterface $decoder
     * @param CommentManager $commentManager
     * @return View
     * @throws \App\Exception\NotExistsException
     * @throws \App\Exception\ForbiddenException
     * @throws \Doctrine\ORM\ORMException
     */
    public function update(Request $request, string $context, string $contextIdentifier, string $identifier, DecoderInterface $decoder, CommentManager $commentManager): View
    {
        $command = new UpdateCommentRequest();

        $data = $decoder->decode($request->getContent(), 'json');

        $form = $this->createForm(UpdateCommentType::class, $command);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $comment = $commentManager->updateComment($context, $contextIdentifier, $identifier, $command);

            return View::create(['comment' => $comment]);
        }

        return View::create(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Delete comment
     *
     * @Route("/{context}/{contextIdentifier}/{identifier}", methods={"DELETE"})
     * @SWG\Parameter(
     *     name="context",
     *     in="path",
     *     description="Context type",
     *     type="string",
     *     enum={"task"},
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="contextIdentifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Parameter(
     *     name="identifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Comment deleted",
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * )
     * @SWG\Tag(name="Comment")
     *
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @param CommentManager $commentManager
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Exception
     */
    public function delete(string $context, string $contextIdentifier, string $identifier, CommentManager $commentManager): View
    {
        $commentManager->deleteComment($context, $contextIdentifier, $identifier);

        return View::create(null, Response::HTTP_OK);
    }

}
