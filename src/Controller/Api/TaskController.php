<?php

namespace App\Controller\Api;

use App\Entity\Task;
use App\Form\Task\CreateTaskType;
use App\Form\Task\UpdateTaskType;
use App\Model\Task\CreateTaskRequest;
use App\Model\Task\UpdateTaskRequest;
use App\Service\TaskManager;
use App\Service\TaskProvider;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * @Route("/api/task")
 */
class TaskController extends Controller
{

    /**
     * Get all tasks
     *
     * @Route("/list", methods={"GET"})
     * @SWG\Response(
     *     response="200",
     *     description="Return all tasks",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Task::class, groups={"simple"})
     *     )
     * )
     * @SWG\Tag(name="Task")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param TaskProvider $taskProvider
     * @return View
     */
    public function list(TaskProvider $taskProvider): View
    {
        return View::create(['tasks' => $taskProvider->getAll()]);
    }

    /**
     * Find tasks, results are paginated
     *
     * @Route("/search", methods={"GET"})
     * @SWG\Parameter(
     *     name="query",
     *     in="query",
     *     description="Search query",
     *     type="string",
     *     required=false,
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     description="Filters",
     *     type="array",
     *     required=false,
     *     @SWG\Items(
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *         )
     *     )
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     description="Sort order",
     *     type="array",
     *     required=false,
     *     @SWG\Items(
     *         type="array",
     *         @SWG\Items(
     *             type="string",
     *         )
     *     )
     * )
     * @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     type="integer",
     *     required=false,
     * )
     * @SWG\Response(
     *     response=200,
     *     description="List of tasks",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Task::class, groups={"simple"})
     *     )
     * )
     * @SWG\Tag(name="Task")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param Request $request
     * @param TaskProvider $taskProvider
     * @return View
     */
    public function search(Request $request, TaskProvider $taskProvider): View
    {
        $query = (string) $request->query->get('query', '');
        $filters = $request->query->get('filters', []);
        $sort = $request->query->get('sort', []);
        $page = $request->query->getInt('page', 1);

        $paginator = $taskProvider->findByCriteria($query, $filters, $sort, $page);

        return View::create([
            'page' => $paginator->getCurrentPage(),
            'pages' => $paginator->getNbPages(),
            'pageSize' => $paginator->getMaxPerPage(),
            'count' => $paginator->getNbResults(),
            'tasks' => $paginator->getCurrentPageResults(),
        ]);
    }

    /**
     * Get task
     *
     * @Route("/{identifier}", methods={"GET"})
     * @SWG\Parameter(
     *     name="identifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return task",
     *     @SWG\Schema(
     *         @Model(type=Task::class, groups={"simple"})
     *     )
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * )
     * @SWG\Tag(name="Task")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param string $identifier
     * @param TaskProvider $taskProvider
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function one(string $identifier, TaskProvider $taskProvider): View
    {
        $task = $taskProvider->getByIdentifier($identifier);

        return View::create(['task' => $task]);
    }

    /**
     * Creates task
     *
     * @Route("/", methods={"POST"})
     * @SWG\Parameter(
     *     name="Task",
     *     in="body",
     *     description="Task to create",
     *     required=true,
     *     @Model(type=CreateTaskRequest::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Create tasks",
     *     @SWG\Schema(
     *         @Model(type=Task::class, groups={"simple"})
     *     )
     * )
     * @SWG\Tag(name="Task")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param Request $request
     * @param TaskManager $taskManager
     * @param DecoderInterface $decoder
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(Request $request, TaskManager $taskManager, DecoderInterface $decoder): View
    {
        $command = new CreateTaskRequest();

        $data = $decoder->decode($request->getContent(), 'json');

        $form = $this->createForm(CreateTaskType::class, $command);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $task = $taskManager->createTask($command);

            return (View::create())->setData(['task' => $task]);
        }

        return View::create(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Update task
     *
     * @Route("/{identifier}", methods={"PATCH"})
     * @SWG\Parameter(
     *     name="Task",
     *     in="body",
     *     description="Task to update",
     *     required=true,
     *     @Model(type=UpdateTaskRequest::class)
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Update tasks",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Task::class, groups={"simple"})
     *     )
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * )
     * @SWG\Tag(name="Task")
     * @FOSRest\View(serializerGroups={"simple"})
     *
     * @param string $identifier
     * @param Request $request
     * @param DecoderInterface $decoder
     * @param TaskManager $taskManager
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function update(string $identifier, Request $request, DecoderInterface $decoder, TaskManager $taskManager): View
    {
        $command = new UpdateTaskRequest();

        $data = $decoder->decode($request->getContent(), 'json');

        $form = $this->createForm(UpdateTaskType::class, $command);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $task = $taskManager->updateTask($identifier, $command);

            return (View::create())->setData(['task' => $task]);
        }

        return View::create(['form' => $form], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Delete task
     *
     * @Route("/{identifier}", methods={"DELETE"})
     * @SWG\Parameter(
     *     name="identifier",
     *     in="path",
     *     description="UUID",
     *     type="string",
     *     required=true,
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Task deleted",
     * )
     * @SWG\Response(
     *     response="403",
     *     description="Forbidden",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Not found",
     * )
     * @SWG\Tag(name="Task")
     *
     * @param string $identifier
     * @param TaskManager $taskManager
     * @return View
     * @throws \App\Exception\ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(string $identifier, TaskManager $taskManager): View
    {
        $taskManager->deleteTask($identifier);

        return View::create(null, Response::HTTP_OK);
    }

}
