<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator as Faker;

class TaskFixtures extends Fixture
{

    /** @var Faker */
    protected $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadTasks($manager);
    }

    public function loadTasks(ObjectManager $manager)
    {
        foreach ($this->getTasksData() as $key => [$title, $description, $status, $createdAt]) {
            $task = (new Task())
                ->setTitle($title)
                ->setDescription($description)
                ->setStatus($status)
                ->setCreatedAt($createdAt)
            ;

            $manager->persist($task);
        }

        $manager->flush();
    }

    public function getTasksData()
    {
        $tasks = [];

        for ($i = 0; $i < 1000; $i++) {
            $tasks [] = [
                $this->faker->realText($maxNbChars = 200, $indexSize = 2), // title
                $this->faker->realText($maxNbChars = 1000, $indexSize = 4), // description
                $i % 3 + 1, // status
                $this->faker->dateTime($max = 'now', $timezone = null), // createdAt
            ];
        }

        return $tasks;
    }

}
