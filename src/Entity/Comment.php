<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(indexes={@ORM\Index(name="identifier_idx", columns={"identifier"})})
 */
class Comment
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Uuid
     * @Groups({"simple"})
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $context;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $contextIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Groups({"simple"})
     */
    protected $content;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"simple"})
     */
    protected $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"simple"})
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->identifier = Uuid::uuid4();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

//    /**
//     * @param string $identifier
//     * @return Comment
//     */
//    public function setIdentifier(string $identifier): Comment
//    {
//        $this->identifier = $identifier;
//
//        return $this;
//    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return $this->context;
    }

    /**
     * @param string $context
     * @return Comment
     */
    public function setContext(string $context): Comment
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return string
     */
    public function getContextIdentifier(): string
    {
        return $this->contextIdentifier;
    }

    /**
     * @param string $contextIdentifier
     * @return Comment
     */
    public function setContextIdentifier(string $contextIdentifier): Comment
    {
        $this->contextIdentifier = $contextIdentifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Comment
     */
    public function setContent(string $content): Comment
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt(\DateTime $createdAt): Comment
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Comment
     */
    public function setUpdatedAt(\DateTime $updatedAt): Comment
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
