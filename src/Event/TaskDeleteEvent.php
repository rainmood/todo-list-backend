<?php

namespace App\Event;

use App\Entity\Task;
use Symfony\Component\EventDispatcher\Event;

class TaskDeleteEvent extends Event
{

    const NAME = 'task.delete';

    /** @var Task */
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

}
