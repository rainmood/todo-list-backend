<?php

namespace App\Event;

use App\Entity\Task;
use Symfony\Component\EventDispatcher\Event;

class TaskParticipantChangeEvent extends Event
{

    const NAME = 'task.participant_change';

    /** @var Task */
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }

}
