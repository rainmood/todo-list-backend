<?php

namespace App\EventListener;

use App\Event\TaskDeleteEvent;
use Monolog\Logger;

class TaskDeleteListener
{

    /** @var Logger */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function handle(TaskDeleteEvent $event)
    {
        $this->logger->addInfo("TaskDeleteEvent {$event->getTask()->getId()}");

        // todo: implement
    }

}
