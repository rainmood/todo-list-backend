<?php

namespace App\EventListener;

use App\Event\TaskParticipantChangeEvent;
use Monolog\Logger;

class TaskParticipantChangeListener
{

    /** @var Logger */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function handle(TaskParticipantChangeEvent $event)
    {
        $this->logger->addInfo("TaskParticipantChangeEvent {$event->getTask()->getId()}");

        // todo: mail sending
    }

}
