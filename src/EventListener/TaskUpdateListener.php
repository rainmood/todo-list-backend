<?php

namespace App\EventListener;

use App\Event\TaskUpdateEvent;
use Monolog\Logger;

class TaskUpdateListener
{

    /** @var Logger */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function handle(TaskUpdateEvent $event)
    {
        $this->logger->addInfo("TaskUpdateEvent {$event->getTask()->getId()}");

        // todo: mail sending
    }

}
