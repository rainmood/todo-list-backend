<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class ForbiddenException extends \Exception
{

    protected $code = Response::HTTP_FORBIDDEN;

}
