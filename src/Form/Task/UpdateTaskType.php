<?php

namespace App\Form\Task;

use App\Model\Task\UpdateTaskRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateTaskType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('PATCH')
            ->add('title', TextType::class)
            ->add('description', TextType::class)
            ->add('status', TextType::class)
        ;
        $builder
            ->get('status')->addModelTransformer(new CallbackTransformer(
                function ($status) {
                    return (int) $status;
                },
                function ($status) {
                    return (int) $status;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateTaskRequest::class,
            'csrf_protection' => false,
        ]);
    }

}
