<?php

namespace App\Model\Comment;

use App\Entity\Comment;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

class CreateCommentRequest
{

    /**
     * @var string
     */
    public $content;

}
