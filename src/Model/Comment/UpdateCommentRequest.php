<?php

namespace App\Model\Comment;

use App\Entity\Comment;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCommentRequest
{

    /**
     * @var string
     */
    public $content;

    /**
     * Named constructor
     * @param Comment $comment
     * @return UpdateCommentRequest
     */
    public static function fromComment(Comment $comment)
    {
        $dto = new static();
        $dto->content = $comment->getContent();

        return $dto;
    }

}
