<?php

namespace App\Model\Task;

use App\Entity\Task;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition()
 */
class CreateTaskRequest
{

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string
     */
    public $description;

}
