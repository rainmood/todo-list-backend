<?php

namespace App\Model\Task;

use App\Entity\Task;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition()
 */
class UpdateTaskRequest
{

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"App\Entity\Task", "getStatuses"})
     */
    public $status;

    /**
     * Named constructor
     * @param Task $task
     * @return UpdateTaskRequest
     */
    public static function fromTask(Task $task)
    {
        $dto = new static();
        $dto->title = $task->getTitle();
        $dto->description = $task->getDescription();
        $dto->status = $task->getStatus();

        return $dto;
    }

}
