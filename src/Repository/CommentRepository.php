<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @return Comment[] Returns an array of Comment objects
     */
    public function findAllByContext(string $context, string $contextIdentifier): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.context = :context')
            ->andWhere('c.contextIdentifier = :contextIdentifier')
            ->setParameters([
                'context' => $context,
                'contextIdentifier' => $contextIdentifier,
            ])
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $identifier
     * @param string $context
     * @param string $contextIdentifier
     * @return Comment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByIdentifierInContext(string $context, string $contextIdentifier, string $identifier): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.identifier = :identifier')
            ->andWhere('c.context = :context')
            ->andWhere('c.contextIdentifier = :contextIdentifier')
            ->setParameters([
                'identifier' => $identifier,
                'context' => $context,
                'contextIdentifier' => $contextIdentifier,
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param Comment $comment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Comment $comment): void
    {
        $this->getEntityManager()->persist($comment);
        $this->getEntityManager()->flush($comment);
    }

    /**
     * @param Comment $comment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(COmment $comment): void
    {
        $this->getEntityManager()->remove($comment);
        $this->getEntityManager()->flush($comment);
    }

}
