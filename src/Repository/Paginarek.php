<?php

namespace App\Repository;

class Paginarek
{

    /** @var array */
    protected $elements;
    /** @var int */
    protected $maxPerPage;
    /** @var int */
    protected $currentPage;

    public function __construct(array $elements)
    {
        $this->elements = $elements;
        $this->maxPerPage = 10;
        $this->currentPage = 1;
    }

    /**
     * @param array $items
     * @param int $page
     * @param int $limit
     * @return Paginarek
     */
    public static function create(array $items, int $page, int $limit): Paginarek
    {
        $paginator = new static($items);
        $paginator->setMaxPerPage($limit);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * @param int $maxPerPage
     * @return $this
     */
    public function setMaxPerPage($maxPerPage): Paginarek
    {
        $this->maxPerPage = $maxPerPage;

        return $this;
    }

    /**
     * @param int $currentPage
     * @return $this
     */
    public function setCurrentPage($currentPage): Paginarek
    {
        if ($currentPage > 0 && $currentPage <= $this->getNbPages()) {
            $this->currentPage = $currentPage;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getCurrentPageResults(): array
    {
        return array_slice($this->elements, $this->countOffset(), $this->maxPerPage);
    }

    /**
     * @return int
     */
    public function countOffset(): int
    {
        if ($this->getCurrentPage() > $this->getNbPages()) {
            $this->currentPage = $this->getNbPages();
        }

        return ($this->getCurrentPage() - 1) * $this->getMaxPerPage();
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getNbPages(): int
    {
        return ceil($this->getNbResults() / $this->getMaxPerPage());
    }

    /**
     * @return int
     */
    public function getMaxPerPage(): int
    {
        return $this->maxPerPage;
    }

    /**
     * @return int
     */
    public function getNbResults(): int
    {
        return count($this->elements);
    }

}
