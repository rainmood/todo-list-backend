<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TaskRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @return string[]
     */
    public function getFieldNames(): array
    {
        $notFilterableColumnsNames = ['id'];
        $columnNames = $this->getEntityManager()->getClassMetadata($this->getEntityName())->getFieldNames();

        return array_diff($columnNames, $notFilterableColumnsNames);
    }

    /**
     * @param Task $task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($task): void
    {
        $this->getEntityManager()->persist($task);
        $this->getEntityManager()->flush($task);
    }

    /**
     * Doctrine result cache
     * @param $identifier
     * @return null|Task
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByIdentifier($identifier): ?Task
    {
        $qb = $this->createQueryBuilder('t');
        return $qb
            ->where('t.identifier = :identifier')->setParameter('identifier', $identifier)
            ->andWhere($qb->expr()->notIn('t.status', [Task::STATUS_REMOVED]))
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->useResultCache(true, 3600, 'task_by_identifier')
            ->getOneOrNullResult();
    }

//    /**
//     * @param array $ids
//     * @return array
//     */
//    public function findById(array $ids): array
//    {
//        $qb = $this->createQueryBuilder('t');
//        return $qb
//            ->where('t.id IN (:ids)')->setParameter('ids', $ids)
//            ->andWhere($qb->expr()->notIn('t.status', [Task::STATUS_REMOVED]))
//            ->orderBy('t.id', 'ASC')
//            ->getQuery()
//            ->getResult();
//    }

    /**
     * @param string $rawQuery
     * @param array $filtersArray
     * @param array $sort
     * @return array
     */
    public function findByCriteria(string $rawQuery, array $filtersArray, array $sort): array
    {
        $qb = $this->createQueryBuilder('t');

        // Query
        $this->applyQuery($qb, $rawQuery);
        // Filters
        $this->applyFilters($qb, $filtersArray);
        // Sort
        $this->applySort($qb, $sort);

        $qb->andWhere($qb->expr()->notIn('t.status', [Task::STATUS_REMOVED]));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param string $rawQuery
     */
    public function applyQuery(QueryBuilder $qb, string $rawQuery): void
    {
        $query = $this->sanitizeSearchQuery($rawQuery);
        if (empty($query)) {
            return;
        }

        $orX = $qb->expr()->orX();
        foreach ($this->getFieldNames() as $column) {
            $exp = $qb->expr()->like("t.$column", $qb->expr()->literal("%$query%"));
            $orX->add($exp);
        }
        $qb->andWhere($orX);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $filtersArray
     */
    public function applyFilters(QueryBuilder $qb, array $filtersArray): void
    {
        if (empty($filtersArray)) {
            return;
        }

        // Validate filter keys
        $filters = array_intersect_key($filtersArray, array_flip($this->getFieldNames()));

//        // Remove special columns
//        $specialColumnsNames = ['status'];
//        $normalColumns = array_diff($columnNames, $specialColumnsNames);
//        foreach ($normalColumns as $column) {
//            if (array_key_exists($column, $filters)) {
//                $qb->andWhere($qb->expr()->like('t.identifier', $qb->expr()->literal('%' . $filters[$column] . '%')));
//            }
//        }

        if (array_key_exists('identifier', $filters)) {
            $qb->andWhere($qb->expr()->like('t.identifier', $qb->expr()->literal('%' . $filters['identifier'] . '%')));
        }
        if (array_key_exists('title', $filters)) {
            $qb->andWhere($qb->expr()->like('t.title', $qb->expr()->literal('%' . $filters['title'] . '%')));
        }
        if (array_key_exists('description', $filters)) {
            $qb->andWhere($qb->expr()->like('t.description', $qb->expr()->literal('%' . $filters['description'] . '%')));
        }
        if (array_key_exists('status', $filters)) {
            $qb->andWhere($qb->expr()->in('t.status', $filters['status']));
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $sort
     */
    public function applySort(QueryBuilder $qb, array $sort): void
    {
        if (empty($sort)) {
            return;
        }

        // Validate
        $sort = array_intersect_key($sort, array_flip($this->getFieldNames()));

        foreach ($sort as $column => $order) {
            if (!in_array(mb_strtoupper($order), ['ASC', 'DESC'])) {
                $order = 'ASC';
            }
            $qb->orderBy("t.$column", $order);
        }
    }

    /**
     * Removes all non-alphanumeric characters except whitespaces.
     * @param string $query
     * @return string
     */
    private function sanitizeSearchQuery(string $query): string
    {
        return trim(preg_replace('/[[:space:]]+/', ' ', $query));
    }

}
