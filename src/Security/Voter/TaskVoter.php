<?php

namespace App\Security\Voter;

use App\Entity\Task;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskVoter extends Voter
{

    const VIEW = 'view';
    const CREATE = 'create';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /** @var AccessDecisionManagerInterface */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, [static::VIEW, static::CREATE, static::EDIT, static::DELETE])) {
            return false;
        }

        if (isset($subject) && !$subject instanceof Task) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Task $subject
     * @param TokenInterface $token
     * @return bool
     * @throws \Exception
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        // todo: implement voter
        return true;

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $task = $subject;

        switch ($attribute) {
            case static::VIEW:
                return $this->canView($task, $user);
            case static::CREATE:
                return $this->canCreate($task, $user);
            case static::EDIT:
                return $this->canEdit($task, $user);
            case static::DELETE:
                return $this->canDelete($task, $user);
            default:
                throw new \Exception('Not existing voter attribute');
        }

        return false;
    }

    /**
     * @param Task $task
     * @param UserInterface $user
     * @return bool
     */
    public function canView(Task $task, UserInterface $user)
    {
        if ($this->canEdit($task, $user)) {
            return true;
        }

        return !$task->isPrivate();
    }

    /**
     * @param Task $task
     * @param UserInterface $user
     * @return bool
     */
    public function canCreate(Task $task, UserInterface $user)
    {
        return true;
    }

    /**
     * @param Task $task
     * @param UserInterface $user
     * @return bool
     */
    public function canEdit(Task $task, UserInterface $user)
    {
        return $task->getOwner() === $user;
    }

    /**
     * @param Task $task
     * @param UserInterface $user
     * @return bool
     */
    public function canDelete(Task $task, UserInterface $user)
    {
        return $task->getOwner() === $user;
    }

}
