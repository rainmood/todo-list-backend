<?php

namespace App\Service;

class CommentContext
{

    const CONTEXT_TASK = 'task';

    /**
     * @return array
     */
    public static function getContextTypes(): array
    {
        return [
            static::CONTEXT_TASK,
        ];
    }

    /**
     * Check if given context is supported
     *
     * @param string $context
     * @return bool
     */
    public static function isSupported(string $context): bool
    {
        return in_array($context, static::getContextTypes(), true);
    }

}
