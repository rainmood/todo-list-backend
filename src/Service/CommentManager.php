<?php

namespace App\Service;

use App\Entity\Comment;
use App\Exception\ForbiddenException;
use App\Exception\NotExistsException;
use App\Model\Comment\CreateCommentRequest;
use App\Model\Comment\UpdateCommentRequest;
use App\Security\Voter\CommentVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CommentManager
{

    /** @var EventDispatcherInterface */
    protected $dispatcher;
    /** @var AuthorizationCheckerInterface */
    protected $security;
    /** @var CommentContext */
    protected $commentContext;
    /** @var CommentProvider */
    protected $commentProvider;

    public function __construct(
        CommentContext $commentContext,
        EventDispatcherInterface $dispatcher,
        AuthorizationCheckerInterface $security,
        CommentProvider $commentProvider
    ) {
        $this->commentContext = $commentContext;
        $this->dispatcher = $dispatcher;
        $this->security = $security;
        $this->commentProvider = $commentProvider;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param CreateCommentRequest $command
     * @return Comment
     * @throws ForbiddenException
     * @throws NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addComment(string $context, string $contextIdentifier, CreateCommentRequest $command): Comment
    {
        if (!$this->commentContext->isSupported($context)) {
            throw new NotExistsException("Not supported context: $context");
        }

        if (!$this->security->isGranted(CommentVoter::CREATE)) {
            throw new ForbiddenException();
        }

        $comment = $this->create($context, $contextIdentifier, $command);

        // $this->dispatcher->dispatch();

        return $comment;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param CreateCommentRequest $command
     * @return Comment
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(string $context, string $contextIdentifier, CreateCommentRequest $command): Comment
    {
        $comment = (new Comment())
            ->setContext($context)
            ->setContextIdentifier($contextIdentifier)
            ->setContent($command->content)
        ;

        $this->commentProvider->save($comment);

        return $comment;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @param UpdateCommentRequest $command
     * @return Comment
     * @throws BadRequestHttpException
     * @throws ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateComment(string $context, string $contextIdentifier, string $identifier, UpdateCommentRequest $command): Comment
    {
        if (!$this->commentContext->isSupported($context)) {
            throw new NotExistsException("Not supported context: $context");
        }

        $comment = $this->commentProvider->findByIdentifierInContext($context, $contextIdentifier, $identifier);

        if (!$this->security->isGranted(CommentVoter::EDIT, $comment)) {
            throw new ForbiddenException();
        }

        $this->update($comment, $command);

        return $comment;
    }

    /**
     * @param Comment $comment
     * @param UpdateCommentRequest $command
     * @return Comment
     * @throws \Doctrine\ORM\ORMException
     */
    public function update(Comment $comment, UpdateCommentRequest $command): Comment
    {
        $comment->setContent($command->content);

        $this->commentProvider->save($comment);

        return $comment;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @throws BadRequestHttpException
     * @throws ForbiddenException
     * @throws \App\Exception\NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteComment(string $context, string $contextIdentifier, string $identifier)
    {
        if (!$this->commentContext->isSupported($context)) {
            throw new NotExistsException("Not supported context: $context");
        }

        $comment = $this->commentProvider->findByIdentifierInContext($context, $contextIdentifier, $identifier);

        if (!$this->security->isGranted(CommentVoter::DELETE, $comment)) {
            throw new ForbiddenException();
        }

        $this->delete($comment);
    }

    /**
     * @param Comment $comment
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Comment $comment)
    {
        $this->commentProvider->remove($comment);
    }

}
