<?php

namespace App\Service;

use App\Entity\Comment;
use App\Exception\ForbiddenException;
use App\Exception\NotExistsException;
use App\Repository\CommentRepository;
use App\Security\Voter\CommentVoter;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CommentProvider
{

    /** @var CommentContext */
    protected $commentContext;
    /** @var CommentRepository */
    protected $commentRepository;
    /** @var AuthorizationCheckerInterface */
    protected $security;

    public function __construct(
        CommentContext $commentContext,
        CommentRepository $commentRepository,
        AuthorizationCheckerInterface $security
    ) {
        $this->commentContext = $commentContext;
        $this->commentRepository = $commentRepository;
        $this->security = $security;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @return Comment[]
     * @throws BadRequestHttpException
     * @throws NotExistsException
     */
    public function getAllByContext(string $context, string $contextIdentifier): array
    {
        if (empty($context) || empty($contextIdentifier)) {
            throw new BadRequestHttpException();
        }
        if (!$this->commentContext->isSupported($context)) {
            throw new NotExistsException("Not supported context: $context");
        }

        $comments = $this->commentRepository->findAllByContext($context, $contextIdentifier);

        $chosenComments = array_filter($comments, function ($comment) {
            return $this->security->isGranted(CommentVoter::VIEW, $comment);
        });

        return $chosenComments;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @return Comment
     * @throws BadRequestHttpException
     * @throws ForbiddenException
     * @throws NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function getByIdentifierInContext(string $context, string $contextIdentifier, string $identifier): Comment
    {
        $comment = $this->findByIdentifierInContext($context, $contextIdentifier, $identifier);

        if (!$this->security->isGranted(CommentVoter::VIEW, $comment)) {
            throw new ForbiddenException();
        }

        return $comment;
    }

    /**
     * @param string $context
     * @param string $contextIdentifier
     * @param string $identifier
     * @return Comment
     * @throws BadRequestHttpException
     * @throws NotExistsException
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByIdentifierInContext(string $context, string $contextIdentifier, string $identifier): Comment
    {
        if (empty($context) || empty($contextIdentifier) || empty($identifier)) {
            throw new BadRequestHttpException();
        }
        if (!CommentContext::isSupported($context)) {
            throw new NotExistsException("Not supported context: $context");
        }

        $comment = $this->commentRepository->findOneByIdentifierInContext($context, $contextIdentifier, $identifier);

        if (!$comment instanceof Comment) {
            throw new NotExistsException();
        }

        return $comment;
    }

    /**
     * @param Comment $comment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Comment $comment): void
    {
        $this->commentRepository->save($comment);
    }

    /**
     * @param Comment $comment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Comment $comment): void
    {
        $this->commentRepository->remove($comment);
    }

}
