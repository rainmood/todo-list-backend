<?php

namespace App\Service;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Filesystem\Filesystem;

class LoggerFactory
{

    /** @var Filesystem */
    protected $filesystem;
    /** @var string */
    protected $logsDir;

    public function __construct(Filesystem $filesystem, string $logsDir)
    {
        $this->filesystem = $filesystem;
        $this->logsDir = $logsDir;
    }

    /**
     * @param string $className
     * @return Logger
     * @throws \Exception
     */
    public function create($className): Logger
    {
        $dir = $this->logsDir($className);

        if (!$this->filesystem->exists($dir)) {
            $this->filesystem->mkdir($dir);
        }

        $logger = new Logger('l');
        $logger->pushHandler(new StreamHandler($dir . $this->logsName()));

        return $logger;
    }

    /**
     * @param string $name
     * @return string
     */
    private function logsDir($name): string
    {
        $name = preg_replace('#[^\\pL\d]+#u', '-', $name);

        return $this->logsDir . '/' . $name . '/';
    }

    /**
     * @return string
     */
    private function logsName(): string
    {
        return date('Y_m_d') . '.log';
    }

}
