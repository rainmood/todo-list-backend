<?php

namespace App\Service;

use App\Entity\Task;
use App\Event\TaskDeleteEvent;
use App\Event\TaskParticipantChangeEvent;
use App\Event\TaskUpdateEvent;
use App\Exception\ForbiddenException;
use App\Model\Task\CreateTaskRequest;
use App\Model\Task\UpdateTaskRequest;
use App\Security\Voter\TaskVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TaskManager
{

    /** @var EventDispatcherInterface */
    protected $dispatcher;
    /** @var AuthorizationCheckerInterface */
    protected $security;
    /** @var TaskProvider */
    protected $taskProvider;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        AuthorizationCheckerInterface $security,
        TaskProvider $taskProvider
    ) {
        $this->dispatcher = $dispatcher;
        $this->security = $security;
        $this->taskProvider = $taskProvider;
    }

    /**
     * @param CreateTaskRequest $createTaskRequest
     * @return Task
     * @throws \Doctrine\ORM\ORMException
     * @throws ForbiddenException
     */
    public function createTask(CreateTaskRequest $createTaskRequest): Task
    {
        if (!$this->security->isGranted(TaskVoter::CREATE)) {
            throw new ForbiddenException();
        }

        $task = $this->create($createTaskRequest);

        $this->dispatcher->dispatch(TaskParticipantChangeEvent::NAME, new TaskParticipantChangeEvent($task));

        return $task;
    }

    /**
     * @param CreateTaskRequest $createTaskRequest
     * @return Task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(CreateTaskRequest $createTaskRequest): Task
    {
        $task = (new Task())
            ->setTitle($createTaskRequest->title)
            ->setDescription($createTaskRequest->description)
            ->setStatus(Task::STATUS_TODO)
        ;

        $this->taskProvider->save($task);

        return $task;
    }

    /**
     * @param string $identifier
     * @param UpdateTaskRequest $updateTaskRequest
     * @throws \Doctrine\ORM\ORMException
     * @return Task
     * @throws ForbiddenException
     * @throws \App\Exception\NotExistsException
     */
    public function updateTask(string $identifier, UpdateTaskRequest $updateTaskRequest): Task
    {
        $task = $this->taskProvider->findByIdentifier($identifier);

        if (!$this->security->isGranted(TaskVoter::EDIT, $task)) {
            throw new ForbiddenException();
        }

        $this->update($task, $updateTaskRequest);

        $this->dispatcher->dispatch(TaskParticipantChangeEvent::NAME, new TaskParticipantChangeEvent($task));
        $this->dispatcher->dispatch(TaskUpdateEvent::NAME, new TaskUpdateEvent($task));

        return $task;
    }

    /**
     * @param Task $task
     * @param UpdateTaskRequest $updateTaskRequest
     * @return Task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Task $task, UpdateTaskRequest $updateTaskRequest): Task
    {
        $task
            ->setTitle($updateTaskRequest->title)
            ->setDescription($updateTaskRequest->description)
            ->setStatus($updateTaskRequest->status ? $updateTaskRequest->status : Task::STATUS_TODO)
        ;

        $this->taskProvider->save($task);

        return $task;
    }

    /**
     * @param string $identifier
     * @throws \Doctrine\ORM\ORMException
     * @return Task
     * @throws ForbiddenException
     * @throws \App\Exception\NotExistsException
     */
    public function deleteTask(string $identifier): Task
    {
        $task = $this->taskProvider->findByIdentifier($identifier);

        if (!$this->security->isGranted(TaskVoter::DELETE, $task)) {
            throw new ForbiddenException();
        }

        $this->delete($task);

        $this->dispatcher->dispatch(TaskDeleteEvent::NAME, new TaskDeleteEvent($task));

        return $task;
    }

    /**
     * @param Task $task
     * @return Task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Task $task): Task
    {
        $task->setStatus(Task::STATUS_REMOVED);

        $this->taskProvider->save($task);

        return $task;
    }

}
