<?php

namespace App\Service;


use App\Entity\Task;
use App\Exception\ForbiddenException;
use App\Exception\NotExistsException;
use App\Repository\Paginarek;
use App\Repository\TaskRepository;
use App\Security\Voter\TaskVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TaskProvider
{

    const LIMIT = 20;

    /** @var TaskRepository */
    protected $taskRepository;
    /** @var AuthorizationCheckerInterface */
    protected $security;

    public function __construct(
        TaskRepository $taskRepository,
        AuthorizationCheckerInterface $security
    ) {
        $this->taskRepository = $taskRepository;
        $this->security = $security;
    }

    /**
     * @param string $identifier
     * @return Task
     * @throws ForbiddenException
     * @throws NotExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByIdentifier(string $identifier): Task
    {
        $task = $this->findByIdentifier($identifier);

        if (!$this->security->isGranted(TaskVoter::VIEW, $task)) {
            throw new ForbiddenException("Task $identifier can not be displayed");
        }

        return $task;
    }

    /**
     * @param string $identifier
     * @return Task
     * @throws NotExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByIdentifier(string $identifier): Task
    {
        if (empty($identifier)) {
            throw new NotExistsException('Empty identifier');
        }

        $task = $this->taskRepository->findByIdentifier($identifier);

        if (!$task instanceof Task) {
            throw new NotExistsException("Task $identifier does not exist");
        }

        return $task;
    }

    /**
     * @return Task[]
     */
    public function getAll(): array
    {
        $tasks = $this->taskRepository->findAll();

        return array_filter($tasks, function ($task) {
            return $this->security->isGranted(TaskVoter::VIEW, $task);
        });
    }

    /**
     * @param string $query
     * @param array $filters
     * @param array $sort
     * @param int $page
     * @return Paginarek
     */
    public function findByCriteria(string $query, array $filters, array $sort, int $page): Paginarek
    {
        $tasks = $this->taskRepository->findByCriteria($query, $filters, $sort);

        $chosenTasks = array_filter($tasks, function ($task) {
            return $this->security->isGranted(TaskVoter::VIEW, $task);
        });

        return Paginarek::create($chosenTasks, $page, static::LIMIT);
    }

    /**
     * @param Task $task
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Task $task)
    {
        $this->taskRepository->save($task);
    }

}
