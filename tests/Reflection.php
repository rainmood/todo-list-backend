<?php

namespace App\Tests;

trait Reflection
{

    /**
     * @param object $entity
     * @param string $propertyName
     * @param mixed $value
     * @throws \ReflectionException
     */
    public function set($entity, $propertyName = 'id', $value)
    {
        $class = new \ReflectionClass($entity);
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);
        $property->setValue($entity, $value);
    }

    /**
     * @param mixed $argument Either a string containing the name of the class to reflect, or an object.
     * @param string $methodName
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    public function getPrivateMethod($argument, $methodName)
    {
        $reflector = new \ReflectionClass($argument);
        $method = $reflector->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * @param string $className
     * @param string $propertyName
     * @return \ReflectionProperty
     * @throws \ReflectionException
     */
    public function getPrivateProperty($className, $propertyName)
    {
        $reflector = new \ReflectionClass($className);
        $property = $reflector->getProperty($propertyName);
        $property->setAccessible(true);

        return $property;
    }

}
