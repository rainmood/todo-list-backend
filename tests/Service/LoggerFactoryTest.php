<?php

namespace App\Tests\Service;

use App\Tests\Reflection;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;

class LoggerFactoryTest extends KernelTestCase
{

    use Reflection;

    public function testCreateSuccess()
    {
        $kernel = static::bootKernel();
        $loggerName = 'test';

        $filesystemMock = $this->getMockBuilder(Filesystem::class)
            ->setMethods(['exists', 'mkdir'])
            ->getMock();

        $filesystemMock->expects($this->once())
            ->method('exists')
            ->will($this->returnValue(false));

        $filesystemMock->expects($this->once())
            ->method('mkdir')
            ->with($this->equalTo(realpath($kernel->getLogDir()) . '/' . $loggerName . '/'));

        $kernel->getContainer()->set('filesystem', $filesystemMock);

        $loggerFactory = $kernel->getContainer()->get('App\Service\LoggerFactory');
        $testLogger = $loggerFactory->create($loggerName);

        $this->assertInstanceOf(Logger::class, $testLogger);
    }

    /**
     * @dataProvider dataProvider
     * @param string $name
     * @param string $expected
     * @throws \ReflectionException
     */
    public function testLogsDirSuccess(string $name, string $expected)
    {
        $kernel = static::bootKernel();
        $loggerFactory = $kernel->getContainer()->get('App\Service\LoggerFactory');
        $logsDir = realpath($kernel->getLogDir());

        $method = $this->getPrivateMethod($loggerFactory, 'logsDir');

        $this->assertSame($logsDir . $expected, $method->invokeArgs($loggerFactory, [$name]));
    }

    public function dataProvider()
    {
        yield ['test', '/test/'];
        yield ['testTest', '/testTest/'];
        yield ['test##$W%BW$@#@51fsdfłąż$RF', '/test-W-BW-51fsdfłąż-RF/'];
    }

    public function testLogsSuccess()
    {
        $kernel = static::bootKernel();
        $loggerFactory = $kernel->getContainer()->get('App\Service\LoggerFactory');

        $stopwatch = (new Stopwatch())->start('test');

        $expected = date('Y_m_d') . '.log';

        $method = $this->getPrivateMethod($loggerFactory, 'logsName');
        $res = $method->invokeArgs($loggerFactory, ['logsDir']);

        $stopwatch->stop('test');

        $this->assertSame($expected, $res);
    }

}
