let Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // assets of the project
    .addEntry('main', './assets/js/main.js')
    .addStyleEntry('styles', './assets/css/styles.scss')

    // uncomment for require $/jQuery as a global variable
    .autoProvidejQuery()

    // Sass/SCSS files
    .enableSassLoader()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())
;

module.exports = Encore.getWebpackConfig();
